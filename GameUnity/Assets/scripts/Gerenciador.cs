﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Gerenciador : MonoBehaviour {

	public Vector2 posicaoInicial;
	public Transform player;
	public int levelAtual;
	public int proximoLevel;
	public int quantidadeTotal;
    public Text textoPontuacaoInicial;
    public Text textoPontuacaoFinal;

    // Use this for initialization
    void Start () {

        PlayerPrefs.SetInt("qtdColetada", 0);

        textoPontuacaoFinal.text = quantidadeTotal.ToString();
        textoPontuacaoInicial.text = "0";


        if (player != null) { 

			posicaoInicial = player.position;
		}
	}
	
	// Update is called once per frame
	void Update () {
        int quantidadeColetada = PlayerPrefs.GetInt("qtdColetada");

        textoPontuacaoInicial.text = quantidadeColetada.ToString();

        if (quantidadeColetada >= quantidadeTotal)
        {
            GameObject.Find("CheckPoint").transform.localScale = new Vector3(891, 891, 0);
        }

      
    }

	public void StartGame(){
		player.position = posicaoInicial;
	}

	public void GameOver(string nome){
		Application.LoadLevel (nome);
	}
    
	public void ProximoLevel(int level){
		Application.LoadLevel ("fase"+level.ToString());
	}
}
