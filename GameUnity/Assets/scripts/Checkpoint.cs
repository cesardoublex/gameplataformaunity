﻿using UnityEngine;
using System.Collections;

public class Checkpoint : MonoBehaviour {

    public int proximoLevel;
 
    void Start () {
        GameObject.Find("CheckPoint").transform.localScale = new Vector3(0, 0, 0);
    }
	
	// Update is called once per frame
	void Update () {       
	
	}

    void OnCollisionEnter2D(Collision2D collision)
    {
        int quantidadeColetada = PlayerPrefs.GetInt("qtdColetada");

        if (collision.gameObject.tag == "Player")
        {
                Application.LoadLevel("fase"+ proximoLevel.ToString());            
        }
    }
}
