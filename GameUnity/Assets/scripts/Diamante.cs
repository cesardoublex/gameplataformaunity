﻿using UnityEngine;
using System.Collections;

public class Diamante : MonoBehaviour {

    public AudioClip som;
	private int quantidadeColetada;
	private Gerenciador gerenciador;
    public AudioSource audio;

    void Walke(){	
		gerenciador = FindObjectOfType (typeof(Gerenciador)) as Gerenciador;
	}

	// Use this for initialization
	void Start () {

    }
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnCollisionEnter2D(Collision2D collision){

        quantidadeColetada = PlayerPrefs.GetInt("qtdColetada");
        
		if (collision.gameObject.tag == "Player") {
            quantidadeColetada++;
            PlayerPrefs.SetInt("qtdColetada", quantidadeColetada);
            audio.PlayOneShot(som);
            Destroy(this.gameObject);
        }
	}
}
  