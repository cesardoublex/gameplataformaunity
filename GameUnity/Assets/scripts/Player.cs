﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {
	
	public float velociadade;
	public Transform player;
	private Animator animacao;

	public bool isGrounded;
	public float force;

	public float jumpTime = 0.10f;
	public float jumpDelay = 0.4f;
	public bool jumped;
	public Transform ground;

	private Gerenciador gerenciador;
    
	// Use this for initialization
	void Start () {
		gerenciador = FindObjectOfType (typeof(Gerenciador)) as Gerenciador;
		animacao = player.GetComponent<Animator> ();
		gerenciador.StartGame ();
	}
	
	// Update is called once per frame
	void Update () {
		
		Movimentar ();

    }
	
	void Movimentar(){		

		isGrounded = Physics2D.Linecast (this.transform.position, ground.position, 1 << LayerMask.NameToLayer ("Plataforma"));


		animacao.SetFloat ("run", Mathf.Abs (Input.GetAxis ("Horizontal")));

		
		if (Input.GetAxisRaw ("Horizontal") > 0) {
			
			transform.Translate (Vector2.right * velociadade * Time.deltaTime);
			transform.eulerAngles = new Vector2(0,0);
		} 
		if (Input.GetAxisRaw ("Horizontal") < 0) {
			
			transform.Translate (Vector2.right * velociadade * Time.deltaTime);
			transform.eulerAngles = new Vector2 (0, 180);
		}

		if (Input.GetButtonDown ("Jump") && isGrounded && !jumped) {
			GetComponent<Rigidbody2D> ().AddForce (transform.up * force);
			jumpTime = jumpDelay;
			animacao.SetTrigger ("jump");
			jumped = true;

		} else {
			animacao.SetTrigger("ground");
		}

		jumpTime -= Time.deltaTime;

		if (jumpTime <= 0 && isGrounded && jumped) {

			animacao.SetTrigger("ground");
			jumped = false;
		}
	}
		
	void onCollisorEnter2D(Collision2D collision){
		if (collision.gameObject.tag.Equals("checkpoint")) {
            if(gerenciador.isColetado() == true) { 
			    gerenciador.ProximoLevel (2);
            }
        }
	}

}

