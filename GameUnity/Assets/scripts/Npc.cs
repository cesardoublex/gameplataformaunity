﻿using UnityEngine;
using System.Collections;

public class Npc : MonoBehaviour {

	public bool isEsquerda = false;
	public float velocidade = 3f;
	public float maxDelay;

	public float maxDistance;
	public float minDistance;

	public float  timeMovimento = 0f;


	void Start () {
	
	}	

	void Update () {
	
		Movimentar ();
	}

	void Movimentar(){

			if (isEsquerda) {
				if(transform.position.x > minDistance){

					transform.Translate (-Vector2.right * velocidade * Time.deltaTime);
					transform.eulerAngles = new Vector2 (0, 0);
				}else{
					isEsquerda = false;
				}

			} else {

			if(transform.position.x < maxDistance){
					transform.Translate (-Vector2.right * velocidade * Time.deltaTime);
					transform.eulerAngles = new Vector2 (0, 180);					
				}else{
					isEsquerda = true;
				}
			}

		
	}
	
}
